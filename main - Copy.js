// Simple three.js example
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';


const vCubeShader=`
varying vec3 vWorldPosition;
uniform float u_rotate;

void main() {
    vec4 rotatedPosition = vec4(1.0, 1.0, u_rotate, 1.0) * vec4(position, 1.0);

    vec4 worldPosition = modelMatrix * rotatedPosition;

  vWorldPosition = vec3(-worldPosition.z, worldPosition.y, -worldPosition.x);

  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
`;

const fCubeShader=`
uniform samplerCube cubemap;
uniform float u_time;
uniform float u_light;
varying vec3 vWorldPosition;

void main(){
	vec3 normalizedVWorldPosition = normalize(vWorldPosition);
  vec3 outcolor = textureCube(cubemap, normalizedVWorldPosition).rgb;

  gl_FragColor = vec4(outcolor * u_light, 1.0);
}
`;


var mesh, renderer, scene, camera, controls;
var cubemap;
var material;
var start = Date.now();
var rotationMatrix = 1.0;
var light = Date.now();
var light_direct = 1;

init();
animate();

function init() {

  // renderer
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  // scene
  scene = new THREE.Scene();

  // camera
  camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 1, 10000);
  camera.position.set(20, 20, 20);

  // controls
  controls = new OrbitControls(camera, renderer.domElement);

  // axes
  //scene.add(new THREE.AxesHelper(20));

  // geometry
  var geometry = new THREE.OctahedronGeometry(5000, 5);

  const loader = new THREE.CubeTextureLoader();
  loader.setPath( 'textures/skybox/' );
  cubemap = loader.load( [
      'space_dn.jpg', 'space_lf.jpg',
      'space_rt.jpg', 'space_up.jpg',
      'space_dn.jpg', 'space_up.jpg'
  ] );

// 	var r = 'https://threejs.org/examples/textures/cube/Bridge2/';
//   var urls = [ r + 'posx.jpg', r + 'negx.jpg',
//               r + 'posy.jpg', r + 'negy.jpg',
//               r + 'posz.jpg', r + 'negz.jpg' ];

//   textureCube = new THREE.CubeTextureLoader().load( urls );

  // material
   material = new THREE.ShaderMaterial({
    side: THREE.BackSide,
    uniforms: {
      cubemap: {
        value: cubemap
      },
      u_time: { value: 0.0 },
      u_light: { value: 0.0 },
      u_rotate: { value: 1.0}
    },


    vertexShader: vCubeShader,
    fragmentShader: fCubeShader
  });

  // mesh
  mesh = new THREE.Mesh(geometry, material);
  scene.add(mesh);
}

function animate() {

  requestAnimationFrame(animate);

  //controls.update();
  if(material != undefined)
  {
      material.uniforms[ 'u_time' ].value = .00025 * ( Date.now() - start );
      
      material.uniforms[ 'u_light' ].value += light_direct * .025;
      if(material.uniforms[ 'u_light' ].value > 2.0 || material.uniforms[ 'u_light' ].value < 0.0)
      {
        light_direct = light_direct * -1;
      }

      console.log("material.uniforms[ 'u_light' ].value = ", material.uniforms[ 'u_light' ].value);
      
      var r = .000025 * ( Date.now() - start );
      material.uniforms[ 'u_rotate' ].value = r + 1.0;

  }
  
  renderer.render(scene, camera);

}
